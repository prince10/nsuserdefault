//
//  ViewController2.m
//  userDefault
//
//  Created by Prince on 17/11/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController2.h"

@interface ViewController2 ()
@property (strong, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)nextPressed:(id)sender {
}

- (IBAction)showUserPressed:(id)sender {
    NSString *showUserName=[[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"USER NAME"
                                                    message:showUserName
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
