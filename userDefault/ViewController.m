//
//  ViewController.m
//  userDefault
//
//  Created by Prince on 17/11/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSString *userName;
    NSString *pass;
    
}

@property (strong, nonatomic) IBOutlet UILabel *userLabel;
@property (strong, nonatomic) IBOutlet UITextField *userTextField;
@property (strong, nonatomic) IBOutlet UILabel *passWordLabel;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;

@end

@implementation ViewController
@synthesize userTextField;
@synthesize passwordTextField;


- (void)viewDidLoad {
    
    NSString *name=[[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
    NSNumber *passNo=[[NSUserDefaults standardUserDefaults]objectForKey:@"pasword"];
    
    NSLog(@" userName   %@",name);
    NSLog(@"passWord    %@",passNo);
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)savePressed:(id)sender {
    
    userName=userTextField.text;
    pass=passwordTextField.text;
    
    
    [[NSUserDefaults standardUserDefaults]setObject:userName forKey:@"name"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:pass forKey:@"pasword"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
}
- (IBAction)show1pressed:(id)sender {
    
    
    
}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
